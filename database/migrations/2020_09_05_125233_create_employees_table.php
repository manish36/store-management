/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("identical_number");
            $table->string("post_title");
            $table->string("department_title");
            $table->string("job_type");
            $table->string("phone");
            $table->string("fax")->nullable()->change();
            $table->string("email")->nullable()->change();
            $table->string("website")->nullable()->change();
            $table->string("province");
            $table->string("district");
            $table->string("municipality");
            $table->string("ward");
            $table->text("town");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
