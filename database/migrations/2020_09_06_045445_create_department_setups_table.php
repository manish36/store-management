/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_setups', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("organization");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_setups');
    }
}
