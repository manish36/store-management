/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_inventories', function (Blueprint $table) {
            $table->id();
            $table->string("age")->nullable()->change();
            $table->string("code")->nullable()->change();
            $table->string("date");
            $table->string("manufacturer");
            $table->string("quantity");
            $table->string("rate");
            $table->string("recieve_number");
            $table->string("remarks")->nullable()->change();
            $table->string("size");
            $table->string("source");
            $table->string("specification");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_inventories');
    }
}
