/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExportersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exporters', function (Blueprint $table) {
            $table->id();
            $table->string("business_name");
            $table->string("person_name");
            $table->string("pan");
            $table->string("phone");
            $table->string("fax")->nullable()->change();
            $table->string("email")->nullable()->change();
            $table->string("website")->nullable()->change();
            $table->string("province");
            $table->string("district");
            $table->string("municipality");
            $table->string("ward");
            $table->text("town");
            $table->string("remarks");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exporters');
    }
}
