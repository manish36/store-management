/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
<?php

namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Model;

class DepartmentSetup extends Model
{
    protected $table = "department_setups";

    /**
     * set the department for employee
     */
    public function products() {
        return $this->hasMany(Employee::class);
    }
}
