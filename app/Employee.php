/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
 
<?php

namespace App;

use App\DepartmentSetup;
use App\EmployeePostSetup;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $table = "employees";
    
    /**
     * get the department for employee
     */
    public function department()
    {
        return $this->belongsTo(DepartmentSetup::class);
    }

    /**
     * get the post for employess
     */
    public function employeePost()
    {
        return $this->belongsTo(EmployeePostSetup::class);
    }
}
