<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

/// Model for product type
class ProductType extends Model
{
    protected $table = "product_types";

     /**
     * set the type for product
     */
    public function products() {
            return $this->hasMany(Product::class);
        }
}
