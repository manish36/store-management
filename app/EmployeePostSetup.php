/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
<?php
namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Model;

/// model for employees post setup in organization
class EmployeePostSetup extends Model
{
    protected $table = "employee_post_setups";
    
    /**
    * set the post for employee
    */
    public function employeePostSetups()
    {
        return $this->hasMany(Employee::class);
    }
}
