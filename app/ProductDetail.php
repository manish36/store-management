<?php

namespace App;

use App\ProductDetail;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    protected $table = "product_details";

    /**
    * get the product type
    */
   public function product_types()
   {
       return $this->belongsTo(ProductType::class);
   }
}
