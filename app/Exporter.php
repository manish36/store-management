/**
 * @author manish adhikari
 * @create date 2020-09-05 17:00:09
 * @modify date 2020-09-05 17:00:21
 */
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/// Model for exproter crud in organization
class Exporter extends Model
{
    //
}
